package com.xitavos.flagder;

/**
 * Created by Logan on 4/07/2015.
 */
public class FlagData
{
    public String title;
    public String author;
    public String description;

    public FlagData(String inTitle, String auth, String desc)
    {
        title = inTitle;
        author = auth;
        description = desc;
    }

}
