package com.xitavos.flagder;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Random;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    TextView mainTextView;
    Button likeButton;
    Button dislikeButton;
    Button infoButton;
    ImageView imageFlag;
    LinearLayout flagContainer;
    public static String imageURL;
    public static String imageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1. Access the TextView defined in layout XML
        // and then set its text
        //mainTextView = (TextView) findViewById(R.id.main_textview);
        //mainTextView.setText("Set in Java!");

        // 2. Access the Button defined in layout XML
        // and listen for it here
        /*likeButton = (Button) findViewById(R.id.like_button);
        likeButton.setOnClickListener(this);
        likeButton.setText("Like");

        dislikeButton = (Button) findViewById(R.id.dislike_button);
        dislikeButton.setText("Dislike");
        dislikeButton.setOnClickListener(this);*/

        infoButton = (Button) findViewById(R.id.info_button);
        infoButton.setText("Info");
        infoButton.setOnClickListener(this);

        imageFlag = (ImageView) findViewById(R.id.main_flag);
        flagContainer = (LinearLayout) findViewById(R.id.flag_container);

        imageFlag.setMaxHeight(flagContainer.getHeight());
        imageFlag.setMaxWidth(flagContainer.getWidth());
        imageFlag.setMinimumWidth(flagContainer.getWidth());

        /*Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        imageFlag.setMinimumWidth(size.x);
        imageFlag.setMaxWidth(size.x);*/

        /*Picasso.with(this)
                .load("http://static2.stuff.co.nz/1412653711/351/10589351.jpg")
                .placeholder(R.drawable.flag)
                .into(imageFlag);*/

        newFlag();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            /*case R.id.like_button:
                like();
                break;

            case R.id.dislike_button:
                dislike();
                break;*/

            case R.id.info_button:
                info();
                break;

        }
    }

    public void like()
    {
        //code
        Toast.makeText(this, "Like", Toast.LENGTH_SHORT).show ();
        newFlag();
    }

    public void dislike()
    {
        //code
        Toast.makeText(this, "Dislike", Toast.LENGTH_SHORT).show ();
        newFlag();
    }

    public void info()
    {
        new AlertDialog.Builder(this)
        .setTitle("Info")
        .setMessage(Html.fromHtml(imageTitle))
        .setNeutralButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
        .setIconAttribute(android.R.attr.alertDialogIcon)
        .show();

        Toast.makeText(this, "Info", Toast.LENGTH_SHORT).show();
    }

    public void newFlag()
    {
        new GrabFlagData().execute();
        //Toast.makeText(this, numberOfFlags, Toast.LENGTH_SHORT).show();
    }

    public void updateImage(String imgURL)
    {
        Picasso.with(this)
                .load(imgURL)
                .placeholder(R.drawable.flag)
                .into(imageFlag);
    }

    private float x1,x2;
    static final int MIN_DISTANCE = 150;

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (deltaX > MIN_DISTANCE)
                {
                    like();
                }
                else if (deltaX < -MIN_DISTANCE)
                {
                    dislike();
                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public class GrabFlagData extends AsyncTask<Void, Void, Void> {
        public String title;
        public String flagURL;
        public String imgURL;
        int totalNumberOfFlags;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //STEP 1: FIND OUT HOW MANY FLAGS THERE ARE
                String URL = "https://www.govt.nz/browse/engaging-with-government/the-nz-flag-your-chance-to-decide/gallery/?start=0&sort=random&scroll=true";
                Document doc = Jsoup.connect(URL).get();
                title = doc.title();
                String[] titleParts = title.split(" ");
                totalNumberOfFlags = Integer.parseInt(titleParts[0]);

                //STEP 2: FIND THE LINK WE NEED
                Random random = new Random();
                int flagID = random.nextInt(totalNumberOfFlags) + 1;
                String startGovUrl = "https://www.govt.nz/browse/engaging-with-government/the-nz-flag-your-chance-to-decide/gallery/?start=" + flagID;
                Document connectDoc = Jsoup.connect(startGovUrl).get();

                //STEP 3: CONNECT TO THE WEBSITE WE WANT TO GRAB DATA FROM
                Elements links = connectDoc.select("a.link-block");
                String link = links.get(0).attr("abs:href");
                flagURL = link;

                Document dataDoc = Jsoup.connect(link).get();

                //STEP 4: GRAB THE DATA
                title = dataDoc.title();
                Elements images = dataDoc.select("img.img-responsive");
                imgURL = images.get(0).attr("abs:src");


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Button info = (Button) findViewById(R.id.info_button);

            //imgURL = the url of the image
            //title = the title of the webpage

            MainActivity.imageURL = imgURL;
            MainActivity.imageTitle = title + "<p></p>" + "<a href=\"" + flagURL + "\">Additional Details</a>";
            //String infoText = imgURL;
            //info.setText(infoText);
            updateImage(imgURL);
        }
    }
}
